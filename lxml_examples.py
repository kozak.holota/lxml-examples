from lxml import etree


def parse_cd_catalog():
    with open('cd_catalog.xml', 'r') as cd_catalog:
        return etree.parse(cd_catalog)


def get_cds_by_country(country):
    catalog = parse_cd_catalog()

    return catalog.xpath(f"//COUNTRY[text()='{country}']/ancestor::CD")

if __name__ == '__main__':
    cds = get_cds_by_country('USA')

    for cd in cds:
        print(f"Artist: {cd.xpath('./ARTIST')[0].text}")
        print(f"Title: {cd.xpath('./TITLE')[0].text}")

